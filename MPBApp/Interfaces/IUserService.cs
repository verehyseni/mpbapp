﻿using MPBApp.Models;
using MPBApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPBApp.Interfaces
{
    public interface IUserService
    {
        Task<User> RegisterData(UserViewModel userViewModel);   
    }
}
