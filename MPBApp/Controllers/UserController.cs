﻿using Microsoft.AspNetCore.Mvc;
using MPBApp.Interfaces;
using MPBApp.Models;
using MPBApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPBApp.Controllers
{

    public class UserController : Controller
    {
        private readonly IUserService userService;

        public UserController(IUserService userService)
        {
            this.userService = userService;
        }

        public async Task<IActionResult> RegisterData(UserViewModel userViewModel)  
        {
            try
            {
                var result = await userService.RegisterData(userViewModel);
                return Redirect("https://online.trusti.org");

            }
            catch (Exception ex)
            {
                return Ok(ex.Message);
            }
        }
    }
}
