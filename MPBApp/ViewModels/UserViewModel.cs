﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MPBApp.ViewModels
{
    public class UserViewModel
    {
        [Required(ErrorMessage = "Emaili nuk mund të jetë i zbrazët")]
        [RegularExpression("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@rks-gov.net$", ErrorMessage = "Emaili nuk është valid")]
        public string Email { get; set; }

        [Required(ErrorMessage = "Emri i pajisjes nuk mund të jetë i zbrazët")]
        public string PcName { get; set; }

        [Required(ErrorMessage = "Fjalëkalimi nuk mund të jetë i zbrazët")]
        public string Password { get; set; } 
    }
}
