﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MPBApp.Migrations
{
    public partial class AddedPcName : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PcName",
                table: "Users",
                type: "nvarchar(max)",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PcName",
                table: "Users");
        }
    }
}
