﻿using Microsoft.AspNetCore.Http;
using MPBApp.Data.Context;
using MPBApp.Interfaces;
using MPBApp.Models;
using MPBApp.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MPBApp.Services
{
    public class UserService : IUserService
    {
        private readonly MPBAppDbContext context; 

        public UserService(MPBAppDbContext context)
        {
            this.context = context;
        }

        public async Task<User> RegisterData(UserViewModel userViewModel)
        {
            User user = new User()
            {
                Email = userViewModel.Email,
                PcName = userViewModel.PcName,
                Password = null
            };

            await context.AddAsync(user);
            await context.SaveChangesAsync();
            return user;
        }
    }
}
